function handler(input) {
    let viewHtml = [];
  
    input.data_base_task.map((item, index) => {
      let dataFields = item.fields;
  
      let view = [
        {
          tag: 'column_set',
          flex_mode: 'none',
          background_style: 'default',
          columns: [
            {
              tag: 'column',
              width: 'weighted',
              weight: 1,
              vertical_align: 'top',
              elements: [
                {
                  tag: 'markdown',
                  content: `[${dataFields.Task ? dataFields.Task[0].text : 'Task log'}](${
                    dataFields.Record_url[0].link
                  })`,
                },
              ],
            },
            {
              tag: 'column',
              width: 'weighted',
              weight: 1,
              vertical_align: 'center',
              elements: [
                {
                  tag: 'markdown',
                  content: dataFields?.Person_request
                    ? `<at id=${dataFields.Person_request[0].id}></at>`
                    : 'Trống',
                  text_align: 'center',
                },
              ],
            },
            {
              tag: 'column',
              width: 'weighted',
              weight: 1,
              vertical_align: 'center',
              elements: [
                {
                  tag: 'markdown',
                  content: dataFields?.Person
                    ? `<at id=${dataFields.Person[0].id}></at>`
                    : 'Trống',
                  text_align: 'center',
                },
              ],
            },
          ],
          horizontal_spacing: 'default',
          action: {},
        },
        {
          tag: 'hr',
        },
      ];
  
      viewHtml = viewHtml.concat(view);
    });
  
    let card = {
      elements: [
        {
          tag: 'column_set',
          flex_mode: 'none',
          background_style: 'grey',
          columns: [
            {
              tag: 'column',
              width: 'weighted',
              weight: 1,
              vertical_align: 'top',
              elements: [
                {
                  tag: 'div',
                  text: {
                    content: 'Task',
                    tag: 'lark_md',
                  },
                },
              ],
            },
            {
              tag: 'column',
              width: 'weighted',
              weight: 1,
              vertical_align: 'center',
              elements: [
                {
                  tag: 'markdown',
                  content: 'Người yêu cầu',
                  text_align: 'center',
                },
              ],
            },
            {
              tag: 'column',
              width: 'weighted',
              weight: 1,
              vertical_align: 'top',
              elements: [
                {
                  tag: 'markdown',
                  content: 'Người xử lý',
                  text_align: 'center',
                },
              ],
            },
          ],
        },
        ...viewHtml,
      ],
      header: {
        template: 'green',
        title: {
          content: `${input.data_wh.sprint} Bắt đầu`,
          tag: 'plain_text',
        },
      },
    };

    // aaaa
    return JSON.stringify(card);
  }